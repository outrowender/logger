<?php

return [
    //server para onde o request deve ser enviado ao detectar um novo log
    'server' => 'http://localhost:8000/api/log/',
    //deve enviar logs quando APP_DEBUG=true
    'reportInDebug' => true,
    //tipos de erros que devem ser ignorados no envio
    'ignore' => ['info']
];

