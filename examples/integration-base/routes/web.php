<?php

use wendrpatrck\cpblogs\Reporter;

Route::get('/', function () {
    return view('welcome');
});

//erro não manipulado
Route::get('/unhandled/exception', function(){

    $user = ClasseQueNaoExiste::func('name', 'wender')->get();

    return 'exception';
})->name('unhandled');

//erro manipulado
Route::get('/handled/exception', function(){

    try {
        $eq = $variavelNula + 5;
    } catch (\Exception $ex) {
        Reporter::catchError($ex, [                
            'message' => 'Erro proposital'                
        ]);
    }

    return 'handled error occured';
})->name('handled');

//log do tipo alerta
Route::get('/log/alert', function(){

    Log::alert('Alerta simples de exemplo');

    return 'Simple log';
})->name('log.alert');
