<?php

return [
    'server' => 'http://localhost:8000/api/log/',
    'reportInDebug' => true,
    'ignore' => ['unauthorized']
];
