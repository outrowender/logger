<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Model\LogAssignment;

class AssignmentCreatedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $logAssignment;

    public function __construct(LogAssignment $logAssignment)
    {
        $this->logAssignment = $logAssignment;
    }

    public function getLogAssignment(){
        return $this->logAssignment;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
