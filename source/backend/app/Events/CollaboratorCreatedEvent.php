<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Model\ProjectCollaborator;

class CollaboratorCreatedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    private $projectCollaborator;

    public function __construct(ProjectCollaborator $collaborator)
    {        
        $this->projectCollaborator = $collaborator;
    }

    public function getProjectCollaborator(){        
        return $this->projectCollaborator;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
