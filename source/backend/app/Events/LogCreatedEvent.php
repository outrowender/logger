<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Model\Log;

class LogCreatedEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $log;

    public function __construct(Log $log)
    {
        $this->log = $log;
    }

    public function getLog(){        
        return $this->log;
    }
    
    public function broadcastOn()
    {
        return new Channel('project.'.$this->log->project_id);
    }

    //dados que devem ser distribuídos em broadcast
    public function broadcastWith()
    {
        return [
            'project_id' => $this->log->project_id,
            'log_id' => $this->log->id,
            'title' => $this->log->title
        ];
    }
}
