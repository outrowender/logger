<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Log;
use App\Model\LogType;
use DB;

class AnalyticsController extends Controller
{
    function logsBySystem(){

        $systems = Log::select(
            DB::raw('distinct system_slug, count(id) as count_log'))
            ->groupBy('system_slug')
            ->orderby('count_log', 'DESC')
            ->get();

        return response()->json($systems);
    }

    function logsbyWeek(){

        $query = Log::select(
            DB::raw('distinct error_code, DATE(created_at) as created_at, count(id) as count'))
                     ->groupBy(DB::raw('error_code, DATE(created_at)'))
                     ->orderBy('created_at','asc')
                     ->get();

        $logs = $query->map(function($item){
            $item->count = $item->count;
            $item->date = $item->created_at;

            unset($item->created_at);

            return $item;
        });

        return response()->json($logs);
    }
}
