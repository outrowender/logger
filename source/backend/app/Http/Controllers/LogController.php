<?php
namespace App\Http\Controllers;

use App\Events\NewLogEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\User;
use App\Model\Log;
use App\Model\Project;
use App\Model\LogComment;
use App\Model\LogOccurrence;
use App\Model\LogAssignment;
use App\Model\ProjectCollaborator;
use DB;

use App\Notifications\LogExceeded;

use Illuminate\Support\Facades\Validator;

use Carbon\Carbon;

class LogController extends Controller
{
    function index()
    {
        $response = Log::select('id', 'project_id', 'title', 'level', 'stage')
        ->orderBy('updated_at', 'desc')
        ->withCount('log_occurences')
        ->with('log_occurences')
        ->get();     

        //remapeia a função incluindo a última atividade
        $response = $response->map(function($item){
            $item->log_occurences_map = $item
            ->log_occurences()
            ->select(DB::raw('date(created_at) as label, count(log_id) as count'))
            ->groupBy('label')
            ->get();
            unset($item->log_occurences);
            return $item;
        });

        return response()->json($response);
    }

    //trata o log para mostrar na lista
    function show(int $logId)
    {
        $dateArray = collect(array_fill(0, 7, null))->map(function($item, $index){
            return Carbon::now()->addDays($index*-1);
        });

        $log = Log::withCount('log_occurences')
        ->withCount('log_assignments')
        ->find($logId);  

        $log->seen = Carbon::now();

        $log->saveQuietly();

        $log->log_occurrences = $dateArray->map(function($date, $index) use ($logId){
            return [
                'value' => LogOccurrence::where('log_id', $logId)
                ->whereDate('created_at', '=', $date->toDateString())
                ->count(),
                'label' => $date->toDateString(),
                'index' => $index
            ];                
        });

        $log->snoozed = $log->log_isSnoozed();
        $log->now = Carbon::now()->format('Y-m-d H:i:s');
        
        return response()->json($log);
    }

    //mostra todos os reponsáveis disponíveis
    function showAvailableAssignments(Log $log){
        $logId = $log->id;
        $project = Project::with(['project_collaborators.user', 'project_collaborators.log_assignments'])->find($log->project_id)->project_collaborators->map(function ($item) use ($logId){
            $item->user_id = $item->user_id;
            $item->name = $item->user->name;
            $item->email = $item->user->email;
            $item->assigned = $item->log_assignments->filter(function ($assign) use ($logId){
                return $assign->log_id == $logId; 
            })->count() > 0;
            unset($item->user, $item->log_assignments);
            return $item;
        });

        return response()->json($project, 200);
    }

    //marca responsáveis para o log indicado
    function newAssignment(Log $log, int $id)
    {  
        $assign = LogAssignment::where([['log_id', '=', $log->id], ['project_collaborator_id', '=', $id]])->first();
        
        if(!$assign){

            try {
                LogAssignment::create([
                    'log_id' => $log->id,       
                    'project_collaborator_id' => $id,
                    'project_collaborator_sender_id' => $this->getCollaboratorId($log->project_id)
                ]); 
            } catch (\Exception $ex) {
                return response()->json(['message'=>$ex->getMessage()], 500);
            }            

            return response()->json(['message'=>'adicionado'], 201);
        }else{
            
            $assign->delete();

            return response()->json(['message'=>'removido'], 200);
        }
    }

    //mostra os comentários do log
    function showComments(int $id){

        $log = Log::with(['log_comments.project_collaborator.user'])->find($id)->log_comments->map(function($item){
            $item->user = $item->project_collaborator->user->name;
            unset($item->project_collaborator);
            return $item;
        });

        return response()->json($log);
    }


    //adiciona ou atualiza um log direto do servidor
    function store(string $api_key, Request $request)
    {
        $project = Project::where('api_key', $api_key)->first();

        if(!$project) return response()->json(['message'=> 'Invalid api key'], 400);
     
        $project->logs()->updateOrCreate(
            [
                'title' => $request->title,
                'level' => $request->level,
                'stage' => $request->stage,
                'handled' => $request->handled
            ],
            [
                'title' => $request->title,
                'level' => $request->level,
                'stage' => $request->stage,
                'handled' => $request->handled,
                'details' => json_encode($request->details)
            ]
        )->touch();

        return response()->json(['message'=>'ok'], 200);
    }

    //marcar o log como corrigido
    function markAsFixed(Log $log){
        $log->is_fixed = Carbon::now();

        $collaboratorId = $this->getCollaboratorId($log->project_id);

        $currentUser = auth()->user();

        if($collaboratorId == null){
            return response()->json(['message' => 'Você não é responsável por esse projeto'], 400);
        }

        $log->log_comments()->create([
            'project_collaborator_id' => $collaboratorId,
            'description' => 'Log marcado como "corrigido", por '.$currentUser->name
        ]);

        $log->saveQuietly();

        return response()->json(['is_fixed'=>$log->is_fixed], 200);
    }

    //sielnciar por x dias
    function snooze_until(Log $log, int $days){

        if($this->getCollaboratorId($log->project_id) == null){
            return response()->json(['message' => 'Você não é responsável por esse projeto'], 400);
        }

        $log->snooze_until = Carbon::now()->addDays($days);
        $log->saveQuietly();

        return response()->json(['snoozed', true]);
    }

    //silenciar enquanto o log não alcança um número x
    function snooze_while(Log $log, int $count){

        if($this->getCollaboratorId($log->project_id) == null){
            return response()->json(['message' => 'Você não é responsável por esse projeto'], 400);
        }

        $log->snooze_while = $count;
        $log->saveQuietly();

        return response()->json(['snoozed', true]);
    }

    function comment(Log $log, Request $request)
    {

        if($this->getCollaboratorId($log->project_id) == null){
            return response()->json(['message' => 'Você não é responsável por esse projeto'], 400);
        }

        $rules = [
            'git_project_name' => ['nullable', 'string', 'max:100'],
            'git_hash_commit' => ['nullable', 'string', 'max:100'],
            'description' => ['required', 'string']
        ];

        $v = Validator::make($request->all(), $rules);

        //Validacao do modelo antes de continuar
        if(!$v->passes()) return response()->json(['message' => 'Conteúdo do formulário é inválido!'], 400);

        $log->log_comments()->create([
            'project_collaborator_id' => $this->getCollaboratorId($log->project_id),
            'description' => $request->description,
            'git_project_name' => $request->git_project_name,
            'git_hash_commit' => $request->git_hash_commit
        ]);
      
        try {
            $log->saveQuietly();
        } catch (\Throwable $th) {
            return response()->json(['message'=> $th->getCode()], 500);
        }

        return response()->json($log, 201);
    }

    protected function getCollaboratorId($projectId){

        $currentUser = auth()->user();

        $collaborator = ProjectCollaborator::where('user_id', $currentUser->id)->where('project_id', $projectId)->first();
    
        if($collaborator){
            return $collaborator->id;
        }else{
            return null;
        }
    }
}
