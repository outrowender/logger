<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Project;
use App\Model\Log;
use App\Model\User;
use App\Model\ProjectCollaborator;
use App\Model\LogOccurrence;
use App\Model\ProjectType;
use Carbon\Carbon;
use DB;

class ProjectController extends Controller
{
    function index(){
        
        $projects = Project::all();

        return response()->json($projects, 200);
    }

    //carrega o histórico principal de cada projeto
    function resume(){
        $projects = DB::select(DB::raw(
            '
            SELECT 
            pt.title type,
            p.slug,
            p.title, 
            count(lnfx.project_id) not_fixed, 
            count(lsn.project_id) snoozed, 
            count(l.project_id) count, 
            max(l.updated_at) last_log 
            FROM projects p
            left join logs lnfx on (lnfx.project_id = p.id and isnull(lnfx.is_fixed))
            left join logs lsn on (lsn.project_id = p.id and (now() < lsn.snooze_until))
            left join logs l on (l.project_id = p.id)
            inner join project_types pt on (p.project_type_id = pt.id)
            group by(p.id)
            order by(last_log) desc
            '
        ));
        return response()->json($projects, 200);
    }

    //carrega os timpos de projeto disponíveis
    function types(){

        $types = ProjectType::all();

        return response()->json($types, 200);
    }

    //cria um novo projeto
    function store(Request $request){

        $project = Project::create([
            'title' => $request->name,
            'project_type_id' => $request->type
        ]);

        return redirect()->action('ProjectController@show', ['slug' => $project->slug]);        
    }

    //exibe o projeto usando o slug
    function show(string $slug){

        $project = Project::with(['project_type'])->where('slug', $slug)->first();
        if(!$project) return response()->json(['message'=>'Projeto não encontrado'], 400);
        $project->makeVisible(['api_key']);

        return response()->json($project, 200);
    }

    //mostra os logs pelo id e faz um filtro
    function showLogs(int $id, Request $request){
     
        $data = $request->all(); 
      
        $query = Log::query();

        //Verificar se algum filtro é enviado na consulta
        //?filter[is_fixed]=notNull&filter[handled]=true&filter[any]=debug
        if(isset($data['filter'])){

            foreach ($data['filter'] as $key => $value) {

                switch ($key) {
                    case 'any':
                        $query
                        ->where('title', 'like', '%'.$value.'%')
                        ->orWhere('level', 'like', '%'.$value.'%')
                        ->orWhere('stage', 'like', '%'.$value.'%');
                    break;

                    case 'snoozed';
                        $query    
                        ->whereNotNull('snooze_until')
                        ->orWhereNotNull('snooze_while');         
                    break;

                    case 'production';
                        $query
                        ->where('stage', 'like', 'production%');         
                    break;

                    case 'for_me':
                    break;
                    
                    default:
                         if($value == 'null'){
                        $query->whereNull($key);
                        }else if ($value == 'notNull'){
                            $query->whereNotNull($key);
                        }else{
                            $query->where($key, $value);
                        } 
                    break;
                }

            }

        }

        //um array de datas usadas para a consulta dos últimos dias de cada log
        $dateArray = collect(array_fill(0, 7, null))->map(function($item, $index){
            return Carbon::now()->addDays($index*-1);
        });
        
        try {
            //faz a consulta mapeando cada log e contando as ocorrêcias
            $project = $query->where('project_id', $id)
            ->withCount('log_occurences')
            ->orderBy('updated_at', 'desc')
            ->get()
            ->map(function($item) use($dateArray){       
                unset($item->details);
                $item->now = Carbon::now()->format('Y-m-d H:i:s');
                $item->log_occurrences = $dateArray->map(function($date, $index) use($item){
                    return [
                        'value' => LogOccurrence::where('log_id', $item->id)->whereDate('created_at', '=', $date->toDateString())->count(),
                        'label' => $date->toDateString(),
                        'index' => $index
                    ];                
                });
                
                return $item;
            });
    
            return response()->json($project, 200);
        } catch (\Exception $ex) {
            return response()->json(['message'=>'Não foi possível realizar a consulta'], 400);
        }

    }

    //mostra os gráficos em linha para cada projeto
    function showAnalitycsLinear(string $slug){

        $currentProject = Project::slug($slug);
        $id = $currentProject->id;
        $projectCount = Log::where('project_id', $id)->count();

        $project = [
            'count' => $projectCount,
            'graphics' => array(
            [
                'label' => 'Exception',
                'data' => array(
                    ['handled' => Log::where('project_id', $id)->where('handled', true)->count()],
                    ['unhandled' => Log::where('project_id', $id)->where('handled', false)->count()]
                )
            ],
            // [
            //     'label' => 'Tipo',
            //     'data' => array(
            //         ['E_ERROR' => 82],
            //         ['E_WARNING' => 10],
            //         ['E_NOTICE' => 8]
            //     )
            // ],
            [
                'label' => 'Corrigidos',
                'data' => array(
                    ['sim' => Log::where('project_id', $id)->whereNotNull('is_fixed')->count()],
                    ['não' => Log::where('project_id', $id)->whereNull('is_fixed')->count()]
                )
            ],
            [
                'label' => 'Ambiente',
                'data' => array(
                    ['Produção' => Log::where('project_id', $id)->where('stage', 'like', 'prod%')->count()],
                    ['Desenvolvimento' => Log::where('project_id', $id)->where('stage', 'not like', 'prod%')->count()]
                )
            ]

            )
                
        ];

        return response()->json($project, 200);
    }

    //mostra o gráfico em linha do tempo de cada dia do projeto
    function showAnalitycsTimeline(string $slug){

        $dateArray = collect(array_fill(0, 15, null))->map(function($item, $index){
            return Carbon::now()->addDays($index*-1);
        });

        $project = Project::slug($slug);

        $results = $dateArray->map(function($date, $index) use ($project){
            return [
                'value' => Log::where('project_id', $project->id)->whereDate('created_at', '=', $date->toDateString())->count(),
                'name' => $date->toDateString(),
                'index' => $index
            ];                
        });

        return response()->json($results);
    }

    function showAssignments(string $slug){
        $project = Project::with(['project_collaborators.user', 'project_collaborators.log_assignments'])->where('slug', $slug)->first()->project_collaborators->map(function ($item){
            $item->user_id = $item->user_id;
            $item->name = $item->user->name;
            $item->email = $item->user->email;
            unset($item->user, $item->log_assignments);
            return $item;
        });

        return response()->json($project, 200); 
    }

    //adiciona um reposável para cada projeto indicado
    function addAssignments(string $slug, Request $request){

        $project = Project::where('slug', $slug)->first();
        $user = User::where('email', $request->email)->first();

        if(!$project || !$user){
            return response()->json(['message'=> 'Usuário não encontrado!'], 400);
        }

        $collaborador = ProjectCollaborator::where('project_id', $project->id)->where('user_id', $user->id)->exists();

        if($collaborador){
            return response()->json(['message'=> 'Responsável já adicionado'], 400);
        }

        ProjectCollaborator::create([
            'project_id' => $project->id,
            'user_id' => $user->id,
            'active' => true
        ]);

        return response()->json(['message'=> $user->name . ' adicionado!'], 200);
    }

    function removeAssignment(string $slug, int $id){

        ProjectCollaborator::destroy($id);

        return response()->json(['message'=> 'ok'], 200);

    }

   
}
