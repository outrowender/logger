<?php

namespace App\Listeners;

use App\Events\AssignmentCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NewAssignmentNotification;

use App\Model\LogAssignment;
use App\Model\ProjectCollaborator;
use App\Model\Log;

class AssignmentCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

    /**
     * Handle the event.
     *
     * @param  AssignmentCreatedEvent  $event
     * @return void
     */
    public function handle(AssignmentCreatedEvent $event)
    {
        $logAssignment = $event->getLogAssignment();

        $collaborator = ProjectCollaborator::with('user')->find($logAssignment->project_collaborator_id)->user;
        $sender = ProjectCollaborator::with('user')->find($logAssignment->project_collaborator_sender_id)->user;
        $logDetails = Log::with('project')->find($logAssignment->log_id);

        Notification::send($collaborator, new NewAssignmentNotification($logDetails, $sender));
    }
}
