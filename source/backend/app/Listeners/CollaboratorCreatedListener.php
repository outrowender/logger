<?php

namespace App\Listeners;

use App\Events\CollaboratorCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Notification;
use App\Notifications\CollaboratorCreatedNotification;

use App\Model\Project;
use App\Model\User;

class CollaboratorCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  CollaboratorCreatedEvent  $event
     * @return void
     */
    public function handle(CollaboratorCreatedEvent $event)
    {
        $collaborator = $event->getProjectCollaborator();

        $user = User::find($collaborator->user_id);

        $project = Project::find($collaborator->project_id);

        Notification::send($user, new CollaboratorCreatedNotification($project));
    }
}
