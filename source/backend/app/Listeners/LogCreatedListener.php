<?php

namespace App\Listeners;

use App\Events\LogCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\LogCreatedNotification;
use Illuminate\Support\Facades\Notification;

use App\Model\Log;
use App\Model\User;
use App\Model\ProjectCollaborator;

class LogCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LogCreatedEvent  $event
     * @return void
     */
    public function handle(LogCreatedEvent $event)
    {
        $log = $event->getLog();
        //cria um novo log
        $log->log_occurences()->create();
        
        //notifica os responsáveis
        $collaborators = ProjectCollaborator::with('user')
        ->with('project')
        ->where('project_id', $log->project_id)
        ->get();

        $users = $collaborators->map(function($item){
            return $item->user;
        });

       Notification::send($users, new LogCreatedNotification($log));

    }
}
