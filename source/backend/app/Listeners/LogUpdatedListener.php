<?php

namespace App\Listeners;

use App\Events\LogUpdatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Model\LogOccurrence;

class LogUpdatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LogUpdatedEvent  $event
     * @return void
     */
    public function handle(LogUpdatedEvent $event)
    {
        $log = $event->getLog();

        //adiciona ocorrencias ao log
        LogOccurrence::create(['log_id'=>$log->id]);

       //$occurrencesCount = LogOccurrence::where('log_id', $log->id)->count();

        //error_log($occurrencesCount);
    }
}
