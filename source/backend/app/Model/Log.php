<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

use Illuminate\Support\Facades\Validator;

use App\Events\LogUpdatedEvent;
use App\Events\LogCreatedEvent;
use Carbon\Carbon;

class Log extends Model
{
    use Notifiable;

    protected $fillable = [
        'project_id', 
        'is_fixed', 
        'snooze_until', 
        'snooze_while', 
        'title', 
        'level',
        'stage',
        'handled',
        'seen',
        'details'
    ];

    protected $rules = [
        'project_id' => ['required', 'numeric'],
        'is_fixed' => ['date'],
        'snooze_until' => ['date'],
        'snooze_while' => ['numeric'],
        'title' => ['required', 'string', 'max:100'],
        'level' => ['required', 'string', 'max:100'],
        'stage' => ['required','string', 'max:100'],        
        'handled' => ['required', 'boolean'],
        'details' => ['required', 'json']     
    ];

    protected $errors;

    protected $dispatchesEvents = [
        'updated' => LogUpdatedEvent::class,
        'created' => LogCreatedEvent::class
    ];

    protected static function boot()
    {
        parent::boot();
    }

    function project()
    {
        return $this->belongsTo('App\Model\Project', 'project_id');
    }

    function log_assignments()
    {
        return $this->hasMany('App\Model\LogAssignment');
    }  

    function log_occurences()
    {
        return $this->hasMany('App\Model\LogOccurrence');
    }

    function log_comments()
    {
        return $this->hasMany('App\Model\LogComment');
    }

    //retorna um boolean indicando se o log está silenciado ou não
    function log_isSnoozed(){
        $snoozed_occurrences = $this->log_occurences_count < $this->snooze_while;
        $snoozed_dates = Carbon::now()->lte($this->snooze_until);
        return $snoozed_occurrences || $snoozed_dates;
    }

    //salva quietinho sem disparar eventos
    public function saveQuietly(array $options = [])
    {
        array_push($options, ['timestamps' => false]);
        return static::withoutEvents(function () use ($options) {
            $this->timestamps = false;
            return $this->save();
        });
    }

    public function validate($data)
    {
        $v = Validator::make($data, $this->rules);

        if(!$v->passes()) $this->errors = $v->errors();

        return $v->passes();
    }

    public function errors(){
        return $this->errors;
    }

}
