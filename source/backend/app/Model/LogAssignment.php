<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use App\Events\AssignmentCreatedEvent;

class LogAssignment extends Model
{
    protected $primaryKey = ['log_id', 'project_collaborator_id'];

    public $incrementing = false;

    protected $fillable = [
        'log_id',
        'project_collaborator_id',
        'project_collaborator_sender_id',
        'created_at'  
    ];

    protected $dispatchesEvents = [
        'created' => AssignmentCreatedEvent::class
    ];

    function log()
    {
        return $this->belongsTo('App\Model\Log', 'log_id');
    }

    function project_collaborator()
    {
        return $this->belongsTo('App\Model\ProjectCollaborator', 'project_collaborator_id');
    }

    function project_collaborator_sender()
    {
        return $this->belongsTo('App\Model\ProjectCollaborator', 'project_collaborator_sender_id');
    }

    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if(!is_array($keys)){
            return parent::setKeysForSaveQuery($query);
        }

        foreach($keys as $keyName){
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    protected function getKeyForSaveQuery($keyName = null)
    {
        if(is_null($keyName)){
           $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }
}