<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LogComment extends Model
{
    protected $fillable = [
        'log_id',
        'project_collaborator_id',
        'description',
        'git_project_name',
        'git_commit_hash',
        'created_at',
        'updated_at'
    ];

    function log()
    {
        return $this->belongsTo('App\Model\Log', 'log_id');
    }

    function project_collaborator()
    {
        return $this->belongsTo('App\Model\ProjectCollaborator', 'project_collaborator_id');
    }
}
