<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LogOccurrence extends Model
{
    protected $fillable = [
        'log_id', 
        'created_at'
    ];

    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query) {
            $query->created_at = Carbon::now();
        });
    }

    function log()
    {
        return $this->belongsTo('App\Model\Log', 'log_id');
    }
}
