<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Project extends Model
{
    protected $fillable = [   
        'slug',
        'project_type_id',
        'title',
        'api_key',
        'created_at',
        'updated_at'
    ];

    protected $hidden = [
        'api_key'
    ];

    protected $rules = [
        'project_type_id' => ['required', 'numeric'],
        'title' => ['required', 'numeric']
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query) {
            if($query->api_key == null){
                $query->api_key = static::getGuid();
            }            
            $query->slug = static::slugify($query->title);
        });
    }
  
    private static function getGuid(){
        return Uuid::generate()->string;
    }

    private static function slugify($text){

        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        $text = trim($text, '-');
        $text = preg_replace('~-+~', '-', $text);
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
    
    //pega a função dentro do escopos
    public function scopeSlug($query, string $slug)
    {
        return $query->where('slug', $slug)->first();
    }

    function project_type()
    {
        return $this->belongsTo('App\Model\ProjectType', 'project_type_id');
    }

    function project_collaborators()
    {
        return $this->hasMany('App\Model\ProjectCollaborator');
    }

    function logs()
    {
        return $this->hasMany('App\Model\Log');
    } 
}
