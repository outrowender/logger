<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Events\CollaboratorCreatedEvent;

class ProjectCollaborator extends Model
{
    protected $fillable = [
        'project_id',
        'user_id',
        'active',
        'created_at',
        'updated_at'
    ];

    protected $dispatchesEvents = [
        'created' => CollaboratorCreatedEvent::class
    ];

    function project()
    {
        return $this->belongsTo('App\Model\Project', 'project_id');
    }

    function user()
    {
        return $this->belongsTo('App\Model\User', 'user_id');
    }

    function log_assignments()
    {
        return $this->hasMany('App\Model\LogAssignment');
    }  
}
