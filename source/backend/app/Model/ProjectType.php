<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProjectType extends Model
{
    protected $fillable = [
        'title',
        'instructions'
    ];

    function project()
    {
        return $this->hasMany('App\Model\Project');
    }
}
