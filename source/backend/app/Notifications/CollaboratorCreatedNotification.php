<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Notifications\Messages\MailMessage;

use App\Model\Project;

class CollaboratorCreatedNotification extends Notification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    private $project;
    
    public function __construct(Project $project)
    {
        $this->queue = 'mail';
        $this->project = $project;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('onlog: Novo responsável ['.$this->project->title.']')
                    ->greeting('Você agora é um dos responsáveis por um projeto')
                    ->line('Você pode ver, gerenciar ou adicionar outras pessoas para trabalhar nesse projeto com você')
                    ->line('Projeto: '.$this->project->title)
                    ->action('Ver no painel', env('APP_WEBCLIENT_URL').'/login/'.base64_encode('/home/'.$this->project->slug.'/inbox'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
