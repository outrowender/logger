<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Model\Log;

class LogCreatedNotification extends Notification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    private $log;

    public function __construct(Log $log)
    {
        $this->log = $log;
        $this->queue = 'mail';        
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $logDetails = json_decode($this->log->details);
        
        return (new MailMessage)
                    ->subject('onlog: Novo evento ['.$this->log->project->title.']')
                    ->greeting('Novo log: '.$this->log->title)
                    ->line($this->log->stage)
                    ->line('')
                    ->line('Gravidade: '. ($this->log->handled?'erro esperado':'erro inesperado'))
                    ->line('Tipo: '.$this->log->level)
                    ->line('Arquivo: '.$logDetails->stacktrace->file.':'.$logDetails->stacktrace->line)
                    ->action('Ver no painel', env('APP_WEBCLIENT_URL').'/login/'.base64_encode('/home/'.$this->log->project->slug.'/inbox/'.$this->log->id));

    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
