<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Model\Log;
use App\Model\Project;
use App\Model\User;

class NewAssignmentNotification extends Notification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    private $log;
    private $sender;

    public function __construct(Log $log, User $projectCollaboratorSender)
    {
        $this->log = $log;
        $this->sender = $projectCollaboratorSender;
        $this->queue = 'mail';
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('onlog: Nova atribuição ['.$this->log->project->title.']')
            ->greeting($this->sender->name.' atribuiu um log à você')
            ->line('['.$this->log->title.']')
            ->line($this->log->stage)
            ->action('Ver no painel', env('APP_WEBCLIENT_URL').'/login/'.base64_encode('/home/'.$this->log->project->slug.'/inbox/'.$this->log->id)); 
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
