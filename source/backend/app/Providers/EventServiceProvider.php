<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;


class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        'App\Events\LogUpdatedEvent' => [
            'App\Listeners\LogUpdatedListener',
        ],

        'App\Events\LogCreatedEvent' => [
            'App\Listeners\LogCreatedListener',
        ],

        'App\Events\AssignmentCreatedEvent' => [
            'App\Listeners\AssignmentCreatedListener',
        ],

        'App\Events\CollaboratorCreatedEvent' => [
            'App\Listeners\CollaboratorCreatedListener',
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
