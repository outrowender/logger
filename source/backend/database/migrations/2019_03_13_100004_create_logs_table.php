<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('project_id')->unsigned();
            $table->datetime('is_fixed')->nullable();
            $table->datetime('snooze_until')->nullable();
            $table->integer('snooze_while')->nullable();
            $table->string('title');
            $table->string('level', 100);
            $table->string('stage', 100);            
            $table->boolean('handled');
            $table->datetime('seen')->nullable();
            $table->json('details')->nullable();
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
