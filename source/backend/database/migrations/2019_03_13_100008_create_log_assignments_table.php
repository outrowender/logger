<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_assignments', function (Blueprint $table) {
            $table->bigInteger('log_id')->unsigned();
            $table->integer('project_collaborator_id')->unsigned();
            $table->integer('project_collaborator_sender_id')->unsigned();;
            $table->timestamps();

            $table->primary(['log_id', 'project_collaborator_id']);

            $table->foreign('log_id')->references('id')->on('logs')->onDelete('cascade');
            $table->foreign('project_collaborator_id')->references('id')->on('project_collaborators')->onDelete('cascade');
            $table->foreign('project_collaborator_sender_id')->references('id')->on('project_collaborators')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_assignments');
    }
}
