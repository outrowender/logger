<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('log_id')->unsigned();
            $table->integer('project_collaborator_id')->unsigned();
            $table->string('description');
            $table->string('git_project_name')->nullable();
            $table->string('git_commit_hash')->nullable();
            $table->timestamps();

            $table->foreign('log_id')->references('id')->on('logs')->onDelete('cascade');
            $table->foreign('project_collaborator_id')->references('id')->on('project_collaborators')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_comments');
    }
}
