<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogResumeView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // DB::statement('CREATE VIEW companiesView AS
        //                 SELECT *,
        //                 (
        //                     SELECT GROUP_CONCAT(DISTINCT id SEPARATOR ',')
        //                     FROM people AS p
        //                     WHERE p.company_id = c.id
        //                 ) AS person_ids
        //                 FROM companies AS c');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

// DELIMITER $$
// CREATE PROCEDURE get_log_story_proc (in log_id int, in limit_count int)
// BEGIN
//    SELECT
// 	log_id,
// 	date(created_at) created,
// 	count(log_id) count
// 	FROM log_occurrences
// 	where log_id = log_id
// 	GROUP BY log_id, created
// 	order by log_id, created
// 	limit limit_count;
// END $$
// DELIMITER ;


// CREATE VIEW log_resume_view AS
// SELECT 
// l.id,
// l.project_id,
// l.title,
// l.level,
// l.stage,
// if(l.stage like 'debug%', true, false) debug,
// if((l.snooze_while >= count(lo.log_id) or l.snooze_until >= now()), true, false) snoozed,
// l.handled,
// l.created_at,
// l.updated_at,
// l.seen,
// count(lo.log_id) log_occurrences_count,
// l.details

// FROM logs l
// left join log_occurrences lo on (l.id = lo.log_id)
// group by l.id
// order by l.project_id, l.updated_at desc
