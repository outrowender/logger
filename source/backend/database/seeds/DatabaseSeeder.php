<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserSeed::class);

        $this->call(ProjectTypeSeed::class);
        $this->call(ProjectSeed::class);
        $this->call(ProjectCollaboratorsSeed::class);
        $this->call(LogSeed::class);

        Model::reguard();
    }
}
