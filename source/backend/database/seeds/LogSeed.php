<?php

use Illuminate\Database\Seeder;
use App\Model\Log;
use Faker\Factory as Faker;
use Carbon\Carbon;

class LogSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $levels = ['E_ERROR','E_WARNING','E_INFO'];
        $servers = ['server-1-sa', 'server-2-na-east', 'server-3-eu-east', 'wender-windows', 'wender-fedora', 'wenders-macbook-pro'];

        $faker = Faker::create();

        for ($i = 1; $i <= 5; $i++) {
            
            $log = Log::create([
                'project_id' => 1,
                'seen' => $faker->boolean?Carbon::now():null,
                'is_fixed' => $faker->boolean?Carbon::now():null,
                'snooze_until' => $faker->boolean?Carbon::now()->addDays(rand(1, 7)):null,
                'snooze_while' => $faker->boolean?rand(1, 100):null,
                'title' => $faker->firstNameMale . '\\' . $faker->firstNameFemale . '\\' . $faker->lastName ,
                'level' => $levels[array_rand($levels)],
                'handled' => $faker->boolean,
                'stage' => $faker->boolean?'debug@'.$servers[array_rand($servers)]:'production@'.$servers[array_rand($servers)],
                'details' => '
                {
                    "app":{
                       "type":"http",
                       "releaseStage":"debug"
                    },
                    "request":{
                       "httpMethod":"GET",
                       "url":"localhost:8001\/unhandled\/exception",
                       "userAgent":"Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.100 Safari\/537.36"
                    },
                    "server":{
                       "software":"PHP 7.1.23 Development Server",
                       "name":"127.0.0.1",
                       "port":"8001",
                       "protocol":"HTTP\/1.1",
                       "storage":"154.50 GB free"
                    },
                    "stacktrace":{
                       "file":"\/Users\/wender.galvao\/Documents\/dev\/crashlitycs\/examples\/integration-base\/routes\/web.php",
                       "line":12,
                       "code":{
                          "9":"\/\/erro n\u00e3o manipulado",
                          "10":"Route::get(\'\/unhandled\/exception\', function(){",
                          "11":null,
                          "12":"user = ClasseQueNaoExiste::func(\'name\', \'wender\')->get();",
                          "13":null,
                          "14":"return \'exception\';",
                          "15":"})->name(\'unhandled\');"
                       }
                    }
                }
                '
                
            ]);

            for ($j=2; $j <= rand(0,10); $j++) { 
                $log->log_occurences()->create();
            }         
            
        }
    }
}
