<?php

use Illuminate\Database\Seeder;
use App\Model\ProjectCollaborator;

class ProjectCollaboratorsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        ProjectCollaborator::create([
            'project_id' => 1,
            'user_id' => 1
        ]);
        ProjectCollaborator::create([
            'project_id' => 1,
            'user_id' => 2
        ]);
        ProjectCollaborator::create([
            'project_id' => 1,
            'user_id' => 3
        ]);
        
    }
}
