<?php

use App\Model\Project;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProjectSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {  
        Project::create([
            'project_type_id' => 1,
            'title' => 'Teste de integração',
            'api_key' => '4b7a8670-a17f-11e9-b9c5-d75c93016c83'
        ]);
    
    }
}
