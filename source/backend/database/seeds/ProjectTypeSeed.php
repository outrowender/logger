<?php

use Illuminate\Database\Seeder;

use App\Model\ProjectType;

class ProjectTypeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectType::create([
            'title' => 'PHP Laravel',
            'instructions' => 'composer require wendrpatrck/onlog'
        ]);
    }
}
