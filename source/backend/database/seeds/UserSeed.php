<?php

use Illuminate\Database\Seeder;
use App\Model\User;
use Faker\Factory as Faker;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'name' => 'Wender Patrick',
            'email' => 'me@wenderpatrick.com',
            'password' => 'Laravel123'
        ]);

        $faker = Faker::create();
        
        User::create([
            'name' => $faker->name,
            'email' => $faker->email,
            'password' => 'Laravel123'
        ]);

        User::create([
            'name' => $faker->name,
            'email' => $faker->email,
            'password' => 'Laravel123'
        ]);
        
    }
}
