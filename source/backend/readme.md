## About cpb-logs 🐞

`php artisan migrate` to create database

`php artisan db:seed` to fill database

`php artisan serve` to run project

`php artisan queue:listen --queue=mail` para rodar a fila de emails

`composer require react/dns:^0.4.19` faz downgrade do pacote de dns instalado com o websockets

`php artisan websockets:serve` para rodar o websockets


O pacote para instalar na projeto se encontra em `https://bitbucket.org/wendrpatrck/cpb-logs`
