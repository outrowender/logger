<?php

use Illuminate\Http\Request;

//rotas não seguradas
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
});

Route::put('/log/{api_key}', 'LogController@store');

Route::get('/unauthorized', 'AuthController@unauthorized')->name('login');

//rotas seguradas
Route::group(['prefix' => '/', 'middleware' => 'auth:api'], function () {

    Route::group(['prefix' => 'auth'], function () {
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::get('me', 'AuthController@me');
    });

    Route::group(['prefix' => 'log'], function () {
        Route::get('/', 'LogController@index');
        Route::get('/{log}', 'LogController@show');
        Route::get('/{id}/comments', 'LogController@showComments');       
        Route::get('/{log}/assignments', 'LogController@showAvailableAssignments');
        Route::put('/{log}/assignments/{id}', 'LogController@newAssignment');
        Route::put('/{log}/mark/fixed', 'LogController@markAsFixed');
        Route::put('/{log}/mark/snooze/until/{days}', 'LogController@snooze_until');
        Route::put('/{log}/mark/snooze/while/{count}', 'LogController@snooze_while');
        Route::post('/{log}/comment', 'LogController@comment');
    });

    Route::group(['prefix' => 'analitycs'], function(){
        Route::get('system/logs', 'AnalyticsController@logsBySystem');
        Route::get('logs/date', 'AnalyticsController@logsbyWeek');
    });

    Route::group(['prefix' => 'projects'], function(){
        Route::get('/', 'ProjectController@index');
        Route::post('/', 'ProjectController@store');
        Route::get('/resume', 'ProjectController@resume');
        Route::get('/types', 'ProjectController@types'); 
        Route::get('/{slug}', 'ProjectController@show');
        Route::get('/{slug}/assignments', 'ProjectController@showAssignments'); 
        Route::post('/{slug}/assignments', 'ProjectController@addAssignments'); 
        Route::delete('/{slug}/assignments/{id}', 'ProjectController@removeAssignment'); 
        Route::get('/{id}/logs', 'ProjectController@showLogs');
        Route::get('/{slug}/analitycs/linear', 'ProjectController@showAnalitycsLinear');         
        Route::get('/{slug}/analitycs/timeline', 'ProjectController@showAnalitycsTimeline');         

    });

});