<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('project.{id}', function ($user, $id) {
    //error_log('$id ='.$id);
    //error_log('$user ='.$user->id);
    return true;
});
