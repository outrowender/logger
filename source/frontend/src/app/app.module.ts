import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { HighlightModule } from 'ngx-highlightjs';
import php from 'highlight.js/lib/languages/php';

import { AppComponent } from './app.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { RouterModule, Routes } from '@angular/router'
import { LoginComponent } from './security/login/login.component'
import { SharedModule } from './shared/shared.module'
import { DialogForgotPasswordComponent } from './security/dialog-forgot-password/dialog-forgot-password.component';
import { AuthGuard } from './security/auth.guard';
import { NotFoundComponent } from './shared/not-found/not-found.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'login/:continue', component: LoginComponent },
  { path: 'home', loadChildren: './home/home.module#HomeModule', canActivate: [AuthGuard] },
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '/404' }
]

export function highlightLanguages() {
  return [
    { name: 'php', func: php }
  ]
}

@NgModule({
  declarations: [AppComponent, LoginComponent, DialogForgotPasswordComponent],
  imports: [
    SharedModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    HighlightModule.forRoot({
      languages: highlightLanguages
    })
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [DialogForgotPasswordComponent]
})
export class AppModule { }
