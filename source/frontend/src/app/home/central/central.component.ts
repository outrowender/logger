import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../services/project.service';

@Component({
  selector: 'app-central',
  templateUrl: './central.component.html',
  styleUrls: ['./central.component.css']
})
export class CentralComponent implements OnInit {

  constructor(private _projectService: ProjectService) { }

  data: any[]

  ngOnInit() {
    this.update()
  }

  update() {
    this._projectService.getResume().subscribe(data => {
      this.data = data
    })
  }

}
