import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from './services/project.service';
import { AuthService } from '../security/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private _activatedRoute: ActivatedRoute, private _projectService: ProjectService, private _authService: AuthService) { }

  ngOnInit() {
    if (this._activatedRoute.children.length == 0) {
      this._authService.redirectToFirstProject()
    } else {
      let route = this._activatedRoute.children[0].snapshot.params['project']
      this._projectService.changeCurrentProjectSlug(route)
    }
  }

}
