import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeHeaderComponent } from './layout/home-header/home-header.component';
import { Routes, RouterModule } from '@angular/router';
import { MaterialFlexModule } from '../material-flex/material-flex.module';
import { HomeComponent } from './home.component';
import { SharedModule } from '../shared/shared.module';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { InboxComponent } from './inbox/inbox.component';
import { ProjectService } from './services/project.service';
import { LogService } from './services/log.service';
import { InboxDetailComponent } from './inbox-detail/inbox-detail.component';
import { InboxDetailOptionsComponent } from './inbox-detail/inbox-detail-options/inbox-detail-options.component';
import { InboxDetailNewCommentComponent } from './inbox-detail/inbox-detail-new-comment/inbox-detail-new-comment.component';
import { InboxDetailCommentsComponent } from './inbox-detail/inbox-detail-comments/inbox-detail-comments.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { InboxCollaboratorsComponent } from './inbox-detail/inbox-collaborators/inbox-collaborators.component';
import { ListDetailsComponent } from './project-details/list-details/list-details.component';
import { CollaboratorsComponent } from './project-details/collaborators/collaborators.component';
import { AnalitycsComponent } from './project-details/analitycs/analitycs.component';
import { CreateProjectComponent } from './project-details/create-project/create-project.component';
import { ProjectInstructionsComponent } from './project-details/create-project/project-instructions/project-instructions.component';
import { CollaboratorsNewComponent } from './project-details/collaborators/collaborators-new/collaborators-new.component';
import { CentralComponent } from './central/central.component';
import { LinearInfoComponent } from './inbox/graphs/linear-info/linear-info.component';
import { TimelineComponent } from './inbox/graphs/timeline/timeline.component';
import { InboxFilterFormComponent } from './inbox/inbox-filter-form/inbox-filter-form.component';
import { InboxListItemComponent } from './inbox/inbox-list-item/inbox-list-item.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      //{ path: ':project', redirectTo: ':project/inbox' },
      { path: ':project/inbox', component: InboxComponent },
      { path: ':project/inbox/:log', component: InboxDetailComponent },
      { path: 'central', component: CentralComponent },
      { path: 'create', component: CreateProjectComponent },
      {
        path: ':project/details', component: ProjectDetailsComponent, children: [
          { path: '', component: ListDetailsComponent },
          { path: 'collaborators', component: CollaboratorsComponent },
          { path: 'analitycs', component: AnalitycsComponent }
        ]
      }
    ]
  }
]

@NgModule({
  declarations: [
    HomeHeaderComponent,
    HomeComponent,
    SidebarComponent,
    InboxComponent,
    InboxDetailComponent,
    InboxDetailOptionsComponent,
    InboxDetailNewCommentComponent,
    InboxDetailCommentsComponent,
    ProjectDetailsComponent,
    InboxCollaboratorsComponent,
    ListDetailsComponent,
    CollaboratorsComponent,
    AnalitycsComponent,
    CreateProjectComponent,
    ProjectInstructionsComponent,
    CollaboratorsNewComponent,
    CentralComponent,
    LinearInfoComponent,
    TimelineComponent,
    InboxFilterFormComponent,
    InboxListItemComponent
  ],
  imports: [
    CommonModule,
    MaterialFlexModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  providers: [
    ProjectService,
    LogService
  ],
  entryComponents: [
    InboxDetailNewCommentComponent,
    InboxCollaboratorsComponent,
    CollaboratorsNewComponent
  ]
})
export class HomeModule { }