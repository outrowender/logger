import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LogDetail } from 'src/app/models/log.model';
import { LogService } from '../../services/log.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormControl } from '@angular/forms';
import { LogAssigment } from 'src/app/models/log-assignment.model';

@Component({
  selector: 'app-inbox-collaborators',
  templateUrl: './inbox-collaborators.component.html',
  styleUrls: ['./inbox-collaborators.component.css']
})
export class InboxCollaboratorsComponent implements OnInit {

  formCollaborator: FormGroup
  listCollaborators: LogAssigment[] = []

  constructor(
    public dialogRef: MatDialogRef<InboxCollaboratorsComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: LogDetail,
    private _logService: LogService,
    private _snackBar: MatSnackBar) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {

    this.getAssignments()

    this.formCollaborator = new FormGroup({
      teste: new FormControl('')
    })
  }

  getAssignments() {
    this._logService.logListAssignments(this._data.id).subscribe(data => this.listCollaborators = data
    )
  }

  switchAssignment(index: number) {
    this._logService.logSwitchAssignment(this._data.id, this.listCollaborators[index].id).subscribe(data => {
      this._snackBar.open(`Responsável ${this.listCollaborators[index].name} ${data.message}!`, 'OK', {
        duration: 2000
      })
    })
  }

  formSubmit() {
    if (this.formCollaborator.valid) {
      console.log('valid')
    }
  }

}
