import { Component, OnInit, Input } from '@angular/core';
import { LogService } from '../../services/log.service';
import { LogComment } from 'src/app/models/log-comment.model';

@Component({
  selector: 'app-inbox-detail-comments',
  templateUrl: './inbox-detail-comments.component.html',
  styleUrls: ['./inbox-detail-comments.component.css']
})
export class InboxDetailCommentsComponent implements OnInit {

  constructor(private _logService: LogService) { }

  @Input() logId: number

  comments: LogComment[] = []

  ngOnInit() {
    this._logService.logListComments(this.logId).subscribe(data => {
      this.comments = data
    })
  }

}
