import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LogService } from '../../services/log.service';
import { LogDetail } from 'src/app/models/log.model';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-inbox-detail-new-comment',
  templateUrl: './inbox-detail-new-comment.component.html',
  styleUrls: ['./inbox-detail-new-comment.component.css']
})
export class InboxDetailNewCommentComponent implements OnInit {

  formComment: FormGroup

  constructor(
    public dialogRef: MatDialogRef<InboxDetailNewCommentComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: LogDetail,
    private _logService: LogService,
    private _snackBar: MatSnackBar) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.formComment = new FormGroup({
      description: new FormControl('', [Validators.required]),
      git_project_name: new FormControl(),
      git_commit_hash: new FormControl()
    })
  }

  formSubmit() {
    if (this.formComment.valid) {
      this._logService.logAddComment(this._data.id, this.formComment.value).subscribe(data => {
        //console.log(data)
        this._snackBar.open('Comentário adicionado', 'OK', {
          duration: 2000
        })
      })
    }
  }

}
