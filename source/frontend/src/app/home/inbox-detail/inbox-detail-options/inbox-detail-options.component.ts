import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LogDetail } from 'src/app/models/log.model';
import { MatDialog } from '@angular/material/dialog';
import { InboxDetailNewCommentComponent } from '../inbox-detail-new-comment/inbox-detail-new-comment.component';
import { LogService } from '../../services/log.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { InboxCollaboratorsComponent } from '../inbox-collaborators/inbox-collaborators.component';

@Component({
  selector: 'app-inbox-detail-options',
  templateUrl: './inbox-detail-options.component.html',
  styleUrls: ['./inbox-detail-options.component.css']
})
export class InboxDetailOptionsComponent implements OnInit {

  constructor(public _dialog: MatDialog, private _logService: LogService, private _snack: MatSnackBar) { }

  @Input() details: LogDetail
  @Output() shoudUpdate = new EventEmitter();

  ngOnInit() { }

  openDialog(): void {
    const dialogRef = this._dialog.open(InboxDetailNewCommentComponent, {
      width: '450px',
      data: this.details
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      this.shoudUpdate.emit({ message: "update" })
    });
  }

  openCollaborators() {
    const dialogRef = this._dialog.open(InboxCollaboratorsComponent, {
      width: '450px',
      data: this.details
    });

    dialogRef.afterClosed().subscribe(result => {
      this.shoudUpdate.emit({ message: "update" })
      //console.log('The dialog was closed');
    });
  }

  markAsFixed() {
    this._logService.logMarkAsFixed(this.details.id).subscribe(data => {
      this.details.is_fixed = data.is_fixed
      this._snack.open('Log marcado como corrigido', 'OK', { duration: 2000 })
    })
  }

  markAsSnoozedUntil(days: number) {
    this._logService.logMarkSnoozedUntil(this.details.id, days).subscribe(data => {
      this.details.snoozed = true
      this._snack.open(`Silenciado por ${days} dias`, 'OK', { duration: 2000 })
    })
  }

  markAsSnoozedWhile(times: number) {
    this._logService.logMarkSnoozedWhile(this.details.id, times).subscribe(data => {
      this.details.snoozed = true
      this._snack.open(`Silenciado até ocorrer ${times}x`, 'OK', { duration: 2000 })
    })
  }

}
