import { Component, OnInit } from '@angular/core';
import { LogService } from '../services/log.service';
import { ActivatedRoute } from '@angular/router';
import { LogDetail } from 'src/app/models/log.model';

@Component({
  selector: 'app-inbox-detail',
  templateUrl: './inbox-detail.component.html',
  styleUrls: ['./inbox-detail.component.css']
})
export class InboxDetailComponent implements OnInit {

  constructor(private _logService: LogService, private _activatedRoute: ActivatedRoute) {
    this.log = this._activatedRoute.snapshot.params['log']
  }

  private log: number

  logDetail: LogDetail
  details: any

  ngOnInit() {
    this.loadData()
  }

  shoudUpdate(event: any) {
    this.loadData()
  }

  loadData() {
    this._logService.logsDetails(this.log).subscribe(data => {
      this.logDetail = data
      this.logDetail.log_occurrences = data.log_occurrences.sort((a, b) => b.index - a.index)
      this.loadDetails(data.details)
    })
  }

  loadDetails(jsonValue: string) {
    let details = JSON.parse(jsonValue)

    this.details = details
  }
}
