import { Component, OnInit, Input } from '@angular/core';
import { ProjectService } from 'src/app/home/services/project.service';

@Component({
  selector: 'app-linear-info',
  templateUrl: './linear-info.component.html',
  styleUrls: ['./linear-info.component.css']
})
export class LinearInfoComponent implements OnInit {

  @Input('slug')
  set slug(value: string) {
    if (value) {
      this.updateData(value)
    }
  }

  graphs: any[]

  ngOnInit() {
  }

  constructor(private _projectService: ProjectService) { }

  updateData(slug: string) {
    this._projectService.getAnalitycsLinear(slug).subscribe(data => {
      this.graphs = data.graphics.map(item => {
        return {
          name: item.label,
          series: item.data.map(series => {
            var obj = Object.keys(series)[0]
            return {
              name: obj,
              value: series[obj]
            }
          })
        }
      })
    })
  }



}
