import { Component, OnInit, Input } from '@angular/core';
import { ProjectService } from 'src/app/home/services/project.service';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {

  @Input('slug')
  set slug(value: string) {
    if (value) {
      this.updateData(value)
    }
  }
  constructor(private _projectService: ProjectService) { }

  ngOnInit() {
  }

  multi: any[]
  maxValue: number = 10

  updateData(slug: string) {
    this._projectService.getAnalitycsTimeline(slug).subscribe(data => {

      //traz o maior valor do array do gráfico para calcular a ltura mínima
      let maxValue = data.map(x => x.value).reduce(function (a, b) {
        return Math.max(a, b);
      })

      //mantém uma altura mínima para o gráfico
      if (maxValue > 10) {
        maxValue = null
      }

      //deficne o valor do gráfico
      this.multi = [{
        name: 'Logs registrados',
        series: data.sort((a, b) => b.index - a.index)
      }]

    })
  }

}
