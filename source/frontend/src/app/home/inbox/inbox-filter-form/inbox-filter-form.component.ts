import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged, switchMap, map } from 'rxjs/operators';

@Component({
  selector: 'app-inbox-filter-form',
  templateUrl: './inbox-filter-form.component.html',
  styleUrls: ['./inbox-filter-form.component.css']
})
export class InboxFilterFormComponent implements OnInit {

  formFilter: FormGroup

  @Output() outerQuery = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
    this.formFilter = new FormGroup({
      any: new FormControl(''),
      production: new FormControl(false),
      seen: new FormControl(false),
      snoozed: new FormControl(false),
      is_fixed: new FormControl(false),
      handled: new FormControl(false),
      for_me: new FormControl(false)
    })

    this.formFilter.valueChanges.pipe(
      debounceTime(500),
      distinctUntilChanged()
    ).subscribe(data => this.buildQuery(data))
  }

  buildQuery(form: any) {

    var finalquery = '?'

    Object.keys(form).forEach(key => {

      switch (key) {
        case 'any':
          if (form[key] != '') {
            finalquery += `filter[${key}]=${form[key]}&`
          }

          break

        case 'is_fixed':
          if (form[key] == true) {
            finalquery += `filter[${key}]=notNull&`
          }

          break

        case 'seen':
          if (form[key] == true) {
            finalquery += `filter[${key}]=null&`
          }

          break

        default:
          if (form[key] == true) {
            finalquery += `filter[${key}]=true&`
          }

          break
      }
    })
    this.outerQuery.emit(finalquery);
  }


}
