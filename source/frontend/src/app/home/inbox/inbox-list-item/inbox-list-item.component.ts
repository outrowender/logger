import { Component, OnInit, Input } from '@angular/core';
import { Log } from 'src/app/models/log.model';

@Component({
  selector: 'app-inbox-list-item',
  templateUrl: './inbox-list-item.component.html',
  styleUrls: ['./inbox-list-item.component.css']
})
export class InboxListItemComponent implements OnInit {


  @Input() logs: Log[] = []

  constructor() { }

  ngOnInit() {
  }

}
