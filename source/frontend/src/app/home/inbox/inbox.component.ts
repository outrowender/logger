import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectService } from '../services/project.service';
import { Project } from 'src/app/models/project.model';
import { LogService } from '../services/log.service';
import { Log } from 'src/app/models/log.model';
import { Location } from '@angular/common'

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {

  currentProject: Project
  logs: Log[] = []

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _projectService: ProjectService,
    private _logService: LogService,
    private _location: Location) {
    this._activatedRoute.paramMap.subscribe(data => {

      let currentp = this._activatedRoute.snapshot.params['project']

      this._projectService.changeCurrentProjectSlug(currentp)

    })
  }

  ngOnInit() {
    this._projectService.currentProject.subscribe(project => {
      this.currentProject = project
      if (project != null) this.loadProject()
    })

    this._projectService.onProjectChange.subscribe(data => {
      if (data != null) this.loadProject()
    })
  }

  //carrega os logs do projeto indicado
  loadProject(filter?: string) {
    this._logService.logsInProject(this.currentProject.id, filter).subscribe(data => {
      //remapeia a lista ordenando a quantidade de ocorrências
      this.logs = data.map(item => {
        item.log_occurrences = item.log_occurrences.sort((a, b) => b.index - a.index)
        return item
      })
      //muda o url da página para o filtro atual
      this._location.go(`home/${this.currentProject.slug}/inbox${filter || ''}`)
    })
  }


  filter(data: string) {
    this.loadProject(data)
  }

}