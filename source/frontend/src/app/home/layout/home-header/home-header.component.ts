import { Component, OnInit } from '@angular/core'
import { AuthService } from 'src/app/security/auth.service'
import { ProjectService } from '../../services/project.service'
import { Project } from 'src/app/models/project.model';

@Component({
  selector: 'app-home-header',
  templateUrl: './home-header.component.html',
  styleUrls: ['./home-header.component.css']
})
export class HomeHeaderComponent implements OnInit {

  constructor(private _authService: AuthService, private _projectService: ProjectService) {
    this._projectService.currentProject.subscribe(data => {
      this.project = data
    })
  }

  list: Project[] = []
  project: Project

  ngOnInit() {
    this._projectService.list().subscribe(data => this.list = data)
  }

  logoff() {
    this._authService.logout('/')
  }
}
