import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProjectService } from 'src/app/home/services/project.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-collaborators-new',
  templateUrl: './collaborators-new.component.html',
  styleUrls: ['./collaborators-new.component.css']
})
export class CollaboratorsNewComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CollaboratorsNewComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _projectService: ProjectService,
    private _snackBar: MatSnackBar) {
    this.project = this.data.project
  }

  formCollaborator: FormGroup
  project: string

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.formCollaborator = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email])
    })
  }

  formSubmit() {
    if (this.formCollaborator.valid) {
      this._projectService.addAssignments(this.project, this.formCollaborator.controls.email.value).subscribe(data => {
        this._snackBar.open(data.message, 'OK', {
          duration: 2000
        })
        this.dialogRef.close()
      })

    }
  }

}
