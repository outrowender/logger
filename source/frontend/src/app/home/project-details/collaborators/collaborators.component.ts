import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { ActivatedRoute } from '@angular/router';
import { LogAssigment } from 'src/app/models/log-assignment.model';
import { MatDialog } from '@angular/material/dialog';
import { CollaboratorsNewComponent } from './collaborators-new/collaborators-new.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-collaborators',
  templateUrl: './collaborators.component.html',
  styleUrls: ['./collaborators.component.css']
})
export class CollaboratorsComponent implements OnInit {

  constructor(private _projectService: ProjectService, private _activatedRoute: ActivatedRoute, public _dialog: MatDialog, private _snackBar: MatSnackBar) { }

  collaborators: LogAssigment[] = []
  project: string
  ngOnInit() {
    this.project = this._activatedRoute.parent.snapshot.params['project']
    this.updateList()
  }

  updateList() {
    this._projectService.detailAssignments(this.project).subscribe(data => this.collaborators = data)
  }

  deleteCollaborator(id: number) {
    this._projectService.removeAssignment(this.project, id).subscribe(data => {
      this._snackBar.open('Responsável removido', 'OK', {
        duration: 2000
      })
      this.updateList()
    })
  }

  openDialog(): void {
    const dialogRef = this._dialog.open(CollaboratorsNewComponent, {
      width: '350px',
      data: { project: this.project }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.updateList()
    });
  }

}
