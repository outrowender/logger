import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ProjectService } from '../../services/project.service';
import { ProjectType, Project } from 'src/app/models/project.model';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent implements OnInit {

  isLinear = false;
  formTipo: FormGroup;
  formNome: FormGroup;

  projectTypes: ProjectType[] = []

  constructor(private _projectService: ProjectService) { }

  types: ProjectType[] = []
  createdProject: Project

  ngOnInit() {
    this.formTipo = new FormGroup({
      type: new FormControl('', [Validators.required])
    });
    this.formNome = new FormGroup({
      name: new FormControl('', [Validators.required])
    });

    this._projectService.getProjectTypes().subscribe(data => {
      this.projectTypes = data
    })
  }

  saveProject(stepper: MatStepper) {
    this._projectService.newProject(this.formTipo.controls.type.value, this.formNome.controls.name.value).subscribe(data => {
      this.createdProject = data
      stepper.next();
    })

  }
}