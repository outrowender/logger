import { Component, OnInit, Input } from '@angular/core';
import { Project } from 'src/app/models/project.model';

@Component({
  selector: 'app-project-instructions',
  templateUrl: './project-instructions.component.html',
  styleUrls: ['./project-instructions.component.css']
})
export class ProjectInstructionsComponent implements OnInit {

  constructor() { }

  @Input() project: Project

  ngOnInit() {
  }

}
