import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { ActivatedRoute } from '@angular/router';
import { Project } from 'src/app/models/project.model';

@Component({
  selector: 'app-list-details',
  templateUrl: './list-details.component.html',
  styleUrls: ['./list-details.component.css']
})
export class ListDetailsComponent implements OnInit {

  constructor(private _projectService: ProjectService, private _activatedRoute: ActivatedRoute) { }

  project: Project

  ngOnInit() {
    let project = this._activatedRoute.snapshot.params['project']
    this._projectService.detail(project).subscribe(data => this.project = data)
  }

}
