import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Log, LogDetail } from 'src/app/models/log.model';
import { LogComment } from 'src/app/models/log-comment.model';
import { LogAssigment } from 'src/app/models/log-assignment.model';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  private api = environment.api
  jsonHeader = { 'Content-Type': 'application/json; charset=utf-8' }

  constructor(private _http: HttpClient) { }

  logsInProject(project: number, filter?: string) {
    return this._http.get<Log[]>(`${this.api}/projects/${project}/logs${filter || ''}`)
  }

  logsDetails(log: number) {
    return this._http.get<LogDetail>(`${this.api}/log/${log}`)
  }

  logMarkAsFixed(log: number) {
    return this._http.put<any>(`${this.api}/log/${log}/mark/fixed`, null)
  }

  logMarkSnoozedUntil(log: number, days: number) {
    return this._http.put<any>(`${this.api}/log/${log}/mark/snooze/until/${days}`, null)
  }

  logMarkSnoozedWhile(log: number, times: number) {
    return this._http.put<any>(`${this.api}/log/${log}/mark/snooze/while/${times}`, null)
  }

  logListComments(log: number) {
    return this._http.get<LogComment[]>(`${this.api}/log/${log}/comments`)
  }

  logAddComment(log: number, data: any) {
    return this._http.post<any>(`${this.api}/log/${log}/comment`, JSON.stringify(data), { headers: this.jsonHeader })
  }

  logListAssignments(log: number) {
    return this._http.get<LogAssigment[]>(`${this.api}/log/${log}/assignments`)
  }

  logSwitchAssignment(log: number, collaborator: number) {
    return this._http.put<any>(`${this.api}/log/${log}/assignments/${collaborator}`, null)
  }
}
