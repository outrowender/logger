import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Project, ProjectType } from 'src/app/models/project.model';
import { BehaviorSubject } from 'rxjs';
import { LogAssigment } from 'src/app/models/log-assignment.model';
import Echo from 'laravel-echo'
import Pusher from 'pusher-js'
import { AuthService } from '../../security/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private api = environment.api
  private currentProjectSubject = new BehaviorSubject<Project>(null)
  private onProjectChangeSubject = new BehaviorSubject<any>(null)

  echo: Echo

  currentProject = this.currentProjectSubject.asObservable()

  onProjectChange = this.onProjectChangeSubject.asObservable()

  jsonHeader = { 'Content-Type': 'application/json; charset=utf-8' }

  constructor(private _http: HttpClient, private _authService: AuthService) {
    //pusher start
    (window as any).pusher = new Pusher(environment.ws.key)

    //echo start
    this.echo = new Echo({
      broadcaster: 'pusher',
      key: environment.ws.key,
      wsHost: environment.ws.host,
      wsPort: environment.ws.port,
      wssPort: environment.ws.wssPort,
      useTLS: environment.ws.useTLS,
      authEndpoint: environment.ws.auth,
      auth: {
        headers: {
          'Authorization': 'Bearer ' + _authService.getToken(),
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json; charset=utf-8'
        }
      }
    })

  }

  //muda o slug de projeto atual
  changeCurrentProjectSlug(slug: string) {

    this.list().subscribe(data => {
      let current = data.find(x => x.slug == slug)
      this.loadSocket(current)
      this.currentProjectSubject.next(current)
    })

  }

  loadSocket(project: Project) {

    //se o canal não estiver aberto
    if (this.echo.connector.channels[`project.${project.id}`] == undefined) {

      this.echo.channel(`project.${project.id}`)
        //escuta os eventos pendurados
        .listen('LogUpdatedEvent', (data) => {

          //lança uma notificação no subscriube
          this.onProjectChangeSubject.next(data)

        })
        .listen('LogCreatedEvent', (data) => {

          this.onProjectChangeSubject.next(data)

        })
    }

  }

  //services
  list() {
    return this._http.get<Project[]>(`${this.api}/projects`)
  }

  getResume() {
    return this._http.get<any[]>(`${this.api}/projects/resume`)
  }

  getAnalitycsLinear(slug: string) {
    return this._http.get<any>(`${this.api}/projects/${slug}/analitycs/linear`)
  }

  getAnalitycsTimeline(slug: string) {
    return this._http.get<any>(`${this.api}/projects/${slug}/analitycs/timeline`)
  }

  detail(slug: string) {
    return this._http.get<Project>(`${this.api}/projects/${slug}`)
  }

  detailAssignments(slug: string) {
    return this._http.get<LogAssigment[]>(`${this.api}/projects/${slug}/assignments`)
  }

  addAssignments(slug: string, email: string) {
    return this._http.post<any>(`${this.api}/projects/${slug}/assignments`, JSON.stringify({ email }), { headers: this.jsonHeader })
  }

  removeAssignment(slug: string, id: number) {
    return this._http.delete<any>(`${this.api}/projects/${slug}/assignments/${id}`)
  }

  getProjectTypes() {
    return this._http.get<ProjectType[]>(`${this.api}/projects/types`)
  }

  newProject(type: number, name: string) {
    return this._http.post<Project>(`${this.api}/projects`, JSON.stringify({ type, name }), { headers: this.jsonHeader })
  }

}