export interface LogAssigment {
    id: number;
    project_id: number;
    user_id: number;
    active: boolean;
    created_at: Date;
    updated_at: Date;
    name: string;
    email: string;
    assigned: boolean;
}