export interface LogComment {
    id: number;
    log_id: number;
    project_collaborator_id: number;
    description: string;
    git_project_name: string;
    git_commit_hash?: any;
    created_at: string;
    updated_at: string;
    user: string;
}
