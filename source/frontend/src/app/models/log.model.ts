export interface Log {
    id: number;
    project_id: number;
    title: string;
    level: string;
    stage: string;
    handled: boolean;
    updated_at: Date;
    seen?: Date;
    now: Date;
    log_occurences_count: number;
    log_occurrences: LogOccurrence[];
}

export interface LogDetail {
    id: number;
    project_id: number;
    is_fixed?: any;
    snooze_until?: any;
    snooze_while?: number;
    title: string;
    level: string;
    handled: boolean;
    description?: string;
    stage: string;
    details: string;
    created_at?: Date;
    updated_at?: Date;
    now: Date;
    log_occurrences: LogOccurrence[];
    log_occurences_count: number;
    log_assignments_count: number;
    snoozed: boolean;
}


export interface LogOccurrence {
    value: number;
    label: string;
    index: number;
}