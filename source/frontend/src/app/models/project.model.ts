export interface Project {
    id: number;
    slug: string;
    project_type_id: number;
    api_key?: string;
    title: string;
    created_at: Date;
    updated_at: Date;
    project_type: ProjectType;
}

export interface ProjectType {
    id: number;
    title: string;
    instructions: string;
    created_at: string;
    updated_at: string;
}
