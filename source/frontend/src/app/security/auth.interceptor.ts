import { HttpRequest, HttpInterceptor, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http'
import { Observable, throwError } from 'rxjs'
import { environment } from 'src/environments/environment'
import { Injectable } from '@angular/core'
import { map, catchError } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from 'src/app/security/auth.service';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable()
export class HeaderInterceptor implements HttpInterceptor {
  constructor(private _snackBar: MatSnackBar, private _authService: AuthService, private _router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (localStorage.getItem(environment.storage_auth_token) === null) {
      return next.handle(req)
    }

    const headers = req.headers.set('Content-Type', 'application/json')

    this._authService.checkAndLogout(this._router.url)

    const authReq = req.clone({
      headers: headers.append(
        'Authorization',
        `Bearer ${localStorage.getItem(environment.storage_auth_token)}`
      )
    })

    return next.handle(authReq).pipe(
      catchError((httpErrorResponse: HttpErrorResponse) => {
        let status = httpErrorResponse.status

        //Status de requisição malfeita:
        if (status == 400) {
          this._snackBar.open(`${httpErrorResponse.statusText} | ${httpErrorResponse.error['message']}`, 'OK', {
            duration: 5000
          })

          return throwError(httpErrorResponse);
        }

        //qualquer outro tipo de erro
        this._snackBar.open(`${httpErrorResponse.message}`, 'OK', {
          duration: 5000
        })

        return throwError(httpErrorResponse);
      })
    )
  }
}
