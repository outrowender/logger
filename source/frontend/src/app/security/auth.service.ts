import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { environment } from 'src/environments/environment'
import { HttpClient } from '@angular/common/http'
import { tap } from 'rxjs/operators'
import { MatSnackBar } from '@angular/material/snack-bar'
import { BehaviorSubject } from 'rxjs'
import { Project } from '../models/project.model';
import * as jwt_decode from "jwt-decode";
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  server: string = environment.api
  jsonHeader = { 'Content-Type': 'application/json; charset=utf-8' }

  private bgContent = new BehaviorSubject<string>(null)
  currentState = this.bgContent.asObservable()

  constructor(
    private _http: HttpClient,
    private _router: Router,
    private _snackBar: MatSnackBar
  ) { }

  //funcao faz login e envia para um url específica caso tenha dado um timout ou receba o link via email
  login(email: string, password: string, continueUrl: string) {

    this._http
      .post<any>(
        `${this.server}/auth/login`,
        JSON.stringify({ email, password }),
        { headers: this.jsonHeader }
      )
      .pipe(
        tap(item => {
          localStorage.setItem(
            environment.storage_auth_token,
            item.access_token
          )
          if (continueUrl == undefined) {
            this.redirectToFirstProject()
          } else {
            this._router.navigate([this.checkAndDecodeB64(continueUrl)])
          }

        })
      )
      .subscribe(
        data => {
          this.bgContent.next(null)
        },
        request => {
          this._snackBar.open(request.message, 'OK', {
            duration: 5000
          })
          this.bgContent.next(null)
        }
      )
  }

  //faz logout e envia para uma rota específica
  logout(oldRoute: string) {
    localStorage.removeItem(environment.storage_auth_token)
    if (oldRoute == undefined || oldRoute == '/' || oldRoute == btoa('/home')) {
      this._router.navigate(['login'])
    } else {
      this._router.navigate(['login', oldRoute])
    }

  }

  //verifica se está logado e faz logoff passando um parametro para continuar de onde parou no login
  checkAndLogout(url: string) {
    if (!this.isLoggedIn()) {
      this.logout(btoa(url))
    }
  }

  checkAndLogin(to: string) {
    if (this.isLoggedIn) {
      if (to != undefined) {
        this._router.navigate([this.checkAndDecodeB64(to)])
      } else {
        this._router.navigate(['home'])
      }

    }
  }

  isLoggedIn(): boolean {

    const token = this.getToken()

    if (token == null) return false

    const helper = new JwtHelperService()

    const isExpired = helper.isTokenExpired(token)

    return !isExpired
  }

  getToken() {
    return localStorage.getItem(environment.storage_auth_token)
  }

  private listProjects() {
    return this._http.get<Project[]>(`${this.server}/projects`)
  }

  redirectToFirstProject() {
    this.listProjects().subscribe(data => {
      this._router.navigate(['home', data[0].slug, 'inbox'])
    })
  }

  checkAndDecodeB64(str) {
    if (str == undefined) return str
    if (str === '' || str.trim() === '') return str
    try {
      return atob(str);
    } catch (err) {
      return str;
    }
  }
}
