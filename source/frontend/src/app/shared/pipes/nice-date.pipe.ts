import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'niceDate'
})
export class NiceDatePipe implements PipeTransform {

  transform(value: string, args: string) {

    var targetDate = new Date(value).getTime()
    var nowDate = new Date(args).getTime()

    var dif = Math.floor(((nowDate - targetDate) / 1000) / 86400);

    if (dif < 30) {
      return convertToNiceDate(value, args);
    } else {
      var datePipe = new DatePipe("en-US");
      value = datePipe.transform(value, 'dd/MM/yyyy');
      return value;
    }
  }
}

function convertToNiceDate(time: string, now: string) {

  var diff = ((new Date(now).getTime() - new Date(time).getTime()) / 1000)

  var daydiff = Math.floor(diff / 86400);

  if (isNaN(daydiff) || daydiff < 0 || daydiff >= 31)
    return '';

  return daydiff == 0 && (
    diff < 60 && "agora" ||
    diff < 120 && "1 min" ||
    diff < 3600 && Math.floor(diff / 60) + " min" ||
    diff < 7200 && "1 h" ||
    diff < 86400 && Math.floor(diff / 3600) + " h") ||
    daydiff == 1 && "ontem" ||
    daydiff < 7 && daydiff + " d" ||
    daydiff < 31 && Math.ceil(daydiff / 7) + " semanas";
}