import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http'
import { HeaderInterceptor } from '../security/auth.interceptor';
import { SvgLinearComponent } from './svg/svg-linear/svg-linear.component'
import { HighlightModule } from 'ngx-highlightjs';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NiceDatePipe } from './pipes/nice-date.pipe';
import { NotFoundComponent } from './not-found/not-found.component';
import { MaterialFlexModule } from '../material-flex/material-flex.module';

@NgModule({
  declarations: [SvgLinearComponent, NiceDatePipe, NotFoundComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HighlightModule,
    NgxChartsModule,

    MaterialFlexModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SvgLinearComponent,
    NiceDatePipe,
    NotFoundComponent,
    HighlightModule,
    NgxChartsModule,

    MaterialFlexModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HeaderInterceptor, multi: true }
  ]
})
export class SharedModule { }
