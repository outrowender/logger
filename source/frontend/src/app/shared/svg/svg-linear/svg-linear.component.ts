import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-svg-linear',
  templateUrl: './svg-linear.component.html',
  styleUrls: ['./svg-linear.component.css']
})
export class SvgLinearComponent implements OnInit {

  @Input() height: number
  @Input() width: number
  @Input() padding: number = 8

  @Input() data: any[]

  content: string

  constructor() { }

  ngOnInit() {
    //calcula a largura de cada intervalo entre os gráficos
    let sectionSize = this.width / (this.data.length - 1)

    //Calcula o valor máximo de cada intervalo para calcular a altura do gráfico
    let maxValue = Math.max.apply(Math, this.data.map(data => data.value)) || 0.1

    //mantém uma altura mínima para o gráfico
    if (maxValue < 10) {
      maxValue = 10
    }

    let multiplier = (this.height / maxValue)

    var str = ''

    //desenha as coordnadas do path do svg
    this.data.forEach((element, index) => {
      if (index == 0) {
        str += `M${this.padding / 2} ${((maxValue - element.value) * multiplier) + (this.padding / 2)} `
      }
      else {
        str += `L ${(index) * sectionSize} ${((maxValue - element.value) * multiplier) + (this.padding / 2)} `
      }
    });

    this.content = str
  }

}
