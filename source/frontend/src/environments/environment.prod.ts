export const environment = {
  production: true,
  api: 'http://localhost:8000/api',
  ws: {
    key: 'my-key',
    host: 'localhost',
    port: '6001',
    wssPort: '443',
    auth: 'http://localhost:8010/broadcasting/auth',
    useTLS: false
  },
  storage_auth_token: 'logger'
}
